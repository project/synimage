# synimage

#### На этот проект не распространяется политика рекомендаций по безопасности Drupal .

> ## ***Неподдерживаемый***
> #### Не поддерживается (т.е. заброшен), и больше не разрабатывается. 
> [Узнайте больше о работе с неподдерживаемыми (заброшенными) проектами](https://www.drupal.org/node/251466)

## Чем занимается?

## Расположение

## Имя модуля

## Требования к платформе

## Нужны модули

## Версии

## Как использовать?

 * Включить модуль
 * Добавить 2 мультимедиа-кнопки в настройке редактора
 * включить 2 фильтра: Сolorbox и Image References и убедиться что последний вниуз

## Основные сервисы

## Доп сервисы